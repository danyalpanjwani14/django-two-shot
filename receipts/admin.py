from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "owner")
    list_filter = ("owner",)
    search_fields = ("name",)


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "number", "owner")
    list_filter = ("owner",)
    search_fields = ("name", "number")


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )
    list_filter = ("purchaser", "category", "account")
    search_fields = (
        "vendor",
        "purchaser__username",
        "category__name",
        "account__name",
    )
