from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

# Create your views here.
from django.views.generic import ListView
from receipts.models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    # Filter receipts to show only those of the logged-in user
    context = {"receipts": receipt}
    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")  # Redirect to the URL with name 'home'
    else:
        form = ReceiptForm()

    return render(request, "receipts/create_receipt.html", {"form": form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect(
                "category_list"
            )  # Redirect to the URL with name 'category_list'
    else:
        form = ExpenseCategoryForm()

    return render(request, "receipts/create_category.html", {"form": form})


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect(
                "account_list"
            )  # Redirect to the URL with name 'account_list'
    else:
        form = AccountForm()

    return render(request, "receipts/create_account.html", {"form": form})
